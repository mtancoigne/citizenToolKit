<?php
class Authorisation {
	//**************************************************************
    // Super Admin Authorisation
    //**************************************************************
    public static function isUserSuperAdmin($userId) {
        $res = false;
        if (! empty($userId)) {
            $account = Element::getElementById($userId, Person::COLLECTION, null, array("roles"));
            $res = Role::isUserSuperAdmin(@$account["roles"]);
        }
        return $res;
    }

    //**************************************************************
    // Super Admin Authorisation
    //**************************************************************
    public static function isUser($userId,$roles) {
        $res = false;
        if (! empty($userId)) {
            $account = Element::getElementById($userId, Person::COLLECTION, null, array("roles"));
            $res = Role::isUser(@$account["roles"],$roles);
        }
        return $res;
    }

 	/**
 	 * Description : Check if user is connect 
     * - to the web interface : communecter.org
     * - or to the mobile interface : meteor.communecter.org
     * @param String $token
     * @param String $accountId
 	 * @return Boolean
 	 */
    public static function isMeteorConnected( $token, $accountId, $tokenName, $test=null ) {
        
        $result = false;
        if($test)
            echo $token;

        $hashedToken = hash_hmac('sha256', $token, $accountId, true);
        $hashedToken = base64_encode($hashedToken);
        
        if( $account = PHDB::findOne( Person::COLLECTION , array("_id" => new MongoId($accountId), "loginTokens.hashedToken" => $hashedToken, 'loginTokens.name' => $tokenName ) ) )
        {
                if($test)
                    var_dump($account);
                Person::saveUserSessionData($account);
                if($test)
                    echo "<br/>".Yii::app()->session['userId'];
                $result = true;
        }
        return $result;
    }

    public static function isValidUser( $user, $pwd ) {
        
        $result = false;
        Person::clearUserSessionData();
        $account = PHDB::findOne(Person::COLLECTION, array( '$or' => array( 
                                                        array("email" => new MongoRegex('/^'.preg_quote(trim($user)).'$/i')),
                                                        array("username" => $user) ) ));
        if( @$account )
        {
            if (Person::checkPassword($pwd, $account)) {
                Person::saveUserSessionData($account);
                Person::updateLoginHistory((String) $account["_id"]);
                $result = true;
            }
        }
        return $result;
    }

    
    /**
     * Return true if the organization can modify his members datas
     * Depends if the params isParentOrganizationAdmin is set to true and if the organization 
     * got a flag canEditMember set to true
     * @param String $organizationId An id of an organization
     * @return boolean True if the organization can edit his members data. False, else.
     */
    public static function canEditMembersData($organizationId) {
        $res = false;
        if (Yii::app()->params['isParentOrganizationAdmin']) {
            $organization = Organization::getById($organizationId);
            if (isset($organization["canEditMember"]) && $organization["canEditMember"])
                $res = true;
        }
        return $res;
    }

    /**
    * Get the authorization for edit an item
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canEditItem($userId, $type, $itemId,$parentType=null,$parentId=null,$deleteProcess=false){
        $res=false;    
        $check = false;

        if(!empty($userId)){
        	//DDA
        	if( $type == Room::COLLECTION || $type == Room::CONTROLLER ||

	            $type == Action::COLLECTION || $type == Action::CONTROLLER || 
	            $type == Proposal::COLLECTION || $type == Proposal::CONTROLLER ) { 

	            if( $parentType == null || $parentId == null ){
	                $elem = Element::getByTypeAndId($type, $itemId);

	                if( empty($elem["preferences"]) && 
	                    empty($elem["preferences"]["private"]) ) {
	                    $parentId = $elem["parentId"];
	                    $parentType = $elem["parentType"];
	                }
	            } 
	            $isDDA = true;
	            $type = ( !empty($parentType) ? $parentType : $type );
	            $itemId = ( !empty($parentId) ? $parentId : $itemId );
	            $check = true;
	        }

	        //Super Admin can do anything
	        if(self::isInterfaceAdmin()  )
	            return true;

	        if ( in_array($type, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION]) ) {
	            //Check if delete pending => can not edit
	            $isStatusDeletePending = Element::isElementStatusDeletePending($type, $itemId);
	            if ($isStatusDeletePending && $deleteProcess === false ) {
	                return false;
	            }
	            //Element admin ?
	            else if (self::isElementAdmin($itemId, $type, $userId)) {
	                return true;
	            //Source admin ?
	            } else if (self::isSourceAdmin($itemId, $type, $userId)) {
	                return true;
	            } else if(@$isDDA == true) {
	                return self::canParticipate($userId, $type, $itemId);
	            }
	        } else if($type == Person::COLLECTION) {
	            if($userId==$itemId)
	                $res = true;
	        } else if($type == City::COLLECTION ) {
	            if($check)
	                $res = self::isLocalCitizen( $userId, ($parentType == City::CONTROLLER) ? $parentId : $itemId ); 
	            else 
	                $res = true;
	        } else if(in_array($type, [Poi::COLLECTION, Classified::COLLECTION, Badge::COLLECTION, Place::COLLECTION, Network::COLLECTION])){
                 $res = self::canEdit($userId, $itemId, $type);
            } 
	           
	        else if($type == Proposal::COLLECTION) 
	           $res = self::canParticipate($userId, $type, $itemId);
	        else if($type == Action::COLLECTION) 
	           $res = self::canParticipate($userId, $type, $itemId);
	        else if($type == Form::ANSWER_COLLECTION){
                $res=Form::canEditAnswerById($itemId);
            }
        }
        
        return $res;
    }

    /**
    * check for any element if a user is either member, contributor, attendee
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canParticipate($userId, $itemType, $itemId, $openEdition=true){
        $res=false;

        if( $userId )
        {   
            if($openEdition)
                $res = Preference::isOpenEdition(Preference::getPreferencesByTypeId($itemId, $itemType));
            //var_dump($res);
            if($res != true){
                if( $itemType == Person::COLLECTION && $itemId == $userId)
                    $res = true;
                if(in_array($itemType, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION]))
                    $res=self::isElementMember($itemId, $itemType, $userId);
                if($itemType == City::COLLECTION) 
                    $res = self::isLocalCitizen($userId, $itemId);
                if($itemType == News::COLLECTION) 
                    $res = true;
                if($itemType == Classified::COLLECTION) 
                    $res = true;
                if($itemType == Proposal::COLLECTION){
                    $el = Element::getElementById($itemId, $itemType, null, array("preferences", "parent", "parentId", "parentType") );
                    if( !empty($el["preferences"]) && 
                                isset($el["preferences"]["private"]) &&
                                $el["preferences"]["private"] == false ){
                        $res = true;
                    }else if( !empty($el["parentType"]) && !empty($el["parentId"]) ){
                        $res = self::canParticipate(Yii::app()->session['userId'], $el["parentType"], $el["parentId"]);
                    }else if(!empty($parent)){
                        foreach($parent as $k => $v){
                            $res=self::canParticipate(Yii::app()->session['userId'], $v["type"], $k);
                        }
                    }
                }
            }
        }
        return $res;
    }
    /**
    * check for any element if a user is either member, contributor, attendee
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canSee($contextType=null, $contextId=null, $openEdition=true){
        $res=false;

        if( isset(Yii::app()->session["userId"]) )
        {   
            $res=self::canEditItem(Yii::app()->session["userId"], $contextType, $contextId);
            if(empty($res))
                $res=self::canParticipate(Yii::app()->session["userId"], $contextType, $contextId);
            if(empty($res))
                $res = Costum::sameFunction("canSee",@$params);
        }
        return $res;
    }
    /*** Simple way to know is interface admin 
    * - Working with session on user
    * --Check user super admin of communecter
    * --Check costum admin
    * --[TODO] Check network admin
    * -- Check source admin 
    **/
    public static function isInterfaceAdmin($id=null, $type=null){

        if( self::isUserSuperAdmin(Yii::app()->session["userId"]) )
            return true;
        if(self::isCostumAdmin())
            return true;
        if(!empty($id) && !empty($type) && self::isSourceAdmin ( $id, $type ,Yii::app()->session["userId"]))
            return true;     

        return false;        
    }

    /**
    * Check if an user is admin of the costum if costum is used
    */
    public static function isCostumAdmin(){
        if(isset(Yii::app()->session['costum'])&&!empty(Yii::app()->session['costum'])){
            if(isset(Yii::app()->session['isCostumAdmin'])&&!empty(Yii::app()->session['isCostumAdmin']))
                return Yii::app()->session['isCostumAdmin'];
        }
        return false;
    }
    /**
     * Return true if the user is source admin of the entity(organization, event, project)
     * @param String the id of the entity
     * @param String the type of the entity
     * @param String the id of the user
     * @return bool 
     */
    public static function isSourceAdmin($idEntity, $typeEntity ,$idUser){
        $res = false ;
        $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)));
        if(!empty($project["source"]["sourceKey"])){
            $user = PHDB::findOne(Person::COLLECTION,array("_id"=>new MongoId($idUser),
                                                        "sourceAdmin" => $entity["source"]["sourceKey"]));
        }
        if(!empty($user))
            $res = true ;
        return $res;
    }
     /**
     * Return true if the entity is in openEdition
     * @param String the id of the entity
     * @param String the type of the entity
     * @return bool 
     */
    public static function isOpenEdition($idEntity, $typeEntity, $preferences=null){
        $res = false ;
        if(empty($preferences)){
            $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)),array('preferences'));
            $preferences = @$entity["preferences"];
        }
        if(!empty($preferences)){
           $res = Preference::isOpenEdition($preferences);
        }
        

        return $res;
    }


    /**
     * Return true if the entity is in openEdition
     * @param String the id of the entity
     * @param String the type of the entity
     * @return bool 
     */
    public static function isOpenData($idEntity, $typeEntity, $preferences=null){
        $res = false ;
        if(empty($preferences)){
            $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)),array('preferences'));
            $preferences = @$entity["preferences"];
        }
        if(!empty($preferences)){
           $res = Preference::isOpenData($preferences);

        }
        

        return $res;
    }


    public static function canEditItemOrOpenEdition($idEntity, $typeEntity, $userId, $parentType=null,$parentId=null){
        $res = false ;
        
        $res = self::isOpenEdition($idEntity, $typeEntity);
        if($res != true)
            $res = self::canEditItem($userId, $typeEntity, $idEntity, $parentType, $parentId);

        return $res;
    }

    /**
     * Return true if the user can delete the element
     * @param type $elementType 
     * @param type $elementId 
     * @param type $userId 
     * @return boolean
     */
    public static function canDeleteElement($elementId, $elementType, $userId, $elt = null) {
        //If open Edition : the element can be deleted
        if ($elementType == Person::COLLECTION && $elementId == $userId){
            $res = true;
        }else{
           $res = self::isOpenEdition($elementId, $elementType);
            if($res != true) {
                //check if the user is super admin
                $res = self::isUser($userId, array(Role::SUPERADMIN, Role::COEDITOR ));
                if ($res != true) {
                    // check if the user can edit the element (admin of the element)
                    $res = self::canEditItem($userId, $elementType, $elementId,null,null,true);

                     if ($res != true && !empty($elt) && 
                            !empty($elt["source"]) && 
                            !empty($elt["source"]["key"]) ) {
                        //$user = PHDB::findOneById(Person::COLLECTION, $userId);
                        $el = Slug::getElementBySlug($elt["source"]["key"], array() );
                        if(!empty($el) )
                            $res = self::isElementAdmin($el["id"], $el["type"] ,$userId);
                    }
                }
            } 
        }

        return $res;
    }

    public static function canSeePrivateElement($links, $type, $id, $creator, $parentType=null, $parentId=null){
        if(!@Yii::app()->session["userId"])
            return false;
        else{
            // SuperAdmin Or costum Admin Or source admin
            if(self::isInterfaceAdmin($id, $type))
                return true;
            //creator access to his creativity
            if(Yii::app()->session["userId"]==$creator)
                return true;
            // attendees and contributors directly access and see the element
            if(!empty($links) && 
                @$links[Link::$linksTypes[$type][Person::COLLECTION]] && 
                @$links[Link::$linksTypes[$type][Person::COLLECTION]][Yii::app()->session["userId"]])
                return true;
            if(!empty($parentType) && !empty($parentId))
                return self::canParticipate(Yii::app()->session["userId"], $parentType, $parentId, false);
            if(Authorisation::isElementMember($id, $type, Yii::app()->session["userId"]))
                return true;

            return false;
        }

    }
     /**
     * Return true if the user is  admin of the entity (organization, event, project)
     * @param String the id of the entity
     * @param String the type of the entity
     * @param String the id of the user
     * @return bool 
     */
    public static function isElementAdmin($elementId, $elementType ,$userId, $checkParent=false){
        $res = false ;
        if( self::isInterfaceAdmin($userId) ) {
            return true;
        } else if(empty($res) && in_array($elementType, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])){
            $eltAdmin = Element::getElementById($elementId, $elementType, null, array("name","links", "parent"));
            if(!empty($eltAdmin)){ 
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if(isset($eltAdmin["links"]) 
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdmin"])
                    && !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdminPending"])
                    )
                    return true;
                // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                else if(isset($eltAdmin["parent"]) && !empty($eltAdmin["parent"]) && empty($checkParent)){
                    foreach($eltAdmin["parent"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
                        }
                        if(!empty($res))
                            return $res;
                    }
                }
            }
        }else if( $elementType == Form::COLLECTION ) {
            //$res = self::isFormAdmin($userId, $elementId);
            $res = true;
        }
        if(empty($checkParent)){
            if(Costum::isSameFunction("isElementAdmin")) {
                $res=Costum::sameFunction("isElementAdmin", array("elementId" => $elementId, "elementType"=>$elementType, "userId"=>$userId) ); 
            }
            
        }
        return $res;
    }
    /**
     * Return true if the user is member of the element
     * @param String the id of the user
     * @param String the elementId of the element
     * @param String the elementType of the element
     * @return boolean true or false
     */
    public static function isElementMember($elementId, $elementType, $userId) {
        $res = false;
        
        //Get the members of the organization : if there is no member then it's a new organization
        //We are in a creation process
        if(in_array($elementType, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])){
            if(self::isElementAdmin($elementId, $elementType ,$userId))
                return true;
            $elt = Element::getElementById($elementId, $elementType, null, array("links", "parent"));
            if(!empty($elt)){ 
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if(isset($elt["links"]) 
                    && isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]])
                    && isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId])
                    && !isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["toBeValidated"])
                    )
                    $res = true;
                // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                else if(empty($res) && isset($elt["parent"]) && !empty($elt["parent"])){
                    foreach($elt["parent"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"]);
                        }
                        if(!empty($res))
                            break;
                    }
                }
            }
        }
        $res=Costum::sameFunction("isElementMember", array("elementId" => $elementId, "elementType"=>$elementType, "userId"=>$userId));
        return $res;
    }

    public static function canEditElementByParent($userId, $elemId,$parentType=null,$parentId=null,$type = null){
        $res = false;
        $elem = Element::getByTypeAndId($type, $elemId);

        if(!empty($elem) && !empty($userId)) {
            // case 1 : superAdmin
            if (self::isUserSuperAdmin($userId)) {
                return true;
            }

            // case 2 : organiser of Survey
            if ( @$elem["organizerType"] == Person::COLLECTION && @$elem["organizerId"] == $userId ) {
                return true;
            }

            // case 3 : admin of parent
            if ( self::canEditItem($userId, $parentId, $parentType) )  {
                return true;
           }
        } else {
            //RAJOUTER UN LOG
            error_log("Problem with survey authorization, surveyId:".@$elemId." & userId:".@$userId);
        }
        return $res;
    }

    //check si le user est creator ou canEditItem du parent 
    public static function canEdit($userId, $id,$type){
        //$res = false;
        $elem = Element::getElementById($id, $type, null, array("parent", "creator"));
        if( !empty($elem) && !empty($userId) ) {
            if($userId == @$elem["creator"])
                return true;
            else if(!empty($elem["parent"])){
                foreach($elem["parent"] as $k => $v){
                    if($v["type"]==Person::COLLECTION && $k== $userId)
                        return true;
                    else if(self::canEditItem($userId, $k, $v["type"]))
                        return true;
                }
            }
        } 
        return false;
    }

    public static function userOwner($userId, $type, $id){
        $res = false;
        $el = Element::getElementById($id, $type, null, array("creator")) ;
        if( @$el && $userId && $userId == @$el["creator"] ) 
            return true;
        
        return $res;
    }
    /**
     * List the user that are admin of the organization
     * @param string $organizationId The organization Id to look for
     * @param boolean $pending : true include the pending admins. By default no.
     * @return type array of person Id
     */
    public static function listAdmins($parentId, $parentType, $pending=false) {
        $res = array(); 

        if (in_array($parentType, [Organization::COLLECTION, Event::COLLECTION, Project::COLLECTION])){     
            $parent = Element::getElementById($parentId, $parentType, null, array("links"));
            $link=Link::$linksTypes[$parentType][Person::COLLECTION];
        }else if ($parentType == Form::COLLECTION){  
            $parent = Form::getLinksById($parentId);
            $link="survey";
        }
        
        if ($users = @$parent["links"][$link]) {
            foreach ($users as $personId => $linkDetail) {
                if (@$linkDetail["isAdmin"] == true) {
                    $userActivated = Role::isUserActivated($personId);
                    if($userActivated){
                        if ($pending) {
                            array_push($res, $personId);
                        } else if (@$linkDetail["isAdminPending"] == null || @$linkDetail["isAdminPending"] == false) {
                            array_push($res, $personId); 
                        }
                    }
                }
            }
        }

        return $res;
    }
     public static function verifCaptcha($captcha, $reponse) {
        function rpHash($value) { 
                    $hash = 5381; 
                    $value = strtoupper($value); 
                    for($i = 0; $i < strlen($value); $i++) { 
                        $hash = (leftShift32($hash, 5) + $hash) + ord(substr($value, $i)); 
                    } 
                    return $hash; 
                } 
         
        // Perform a 32bit left shift 
        function leftShift32($number, $steps) { 
            // convert to binary (string) 
            $binary = decbin($number); 
            // left-pad with 0's if necessary 
            $binary = str_pad($binary, 32, "0", STR_PAD_LEFT); 
            // left shift manually 
            $binary = $binary.str_repeat("0", $steps); 
            // get the last 32 bits 
            $binary = substr($binary, strlen($binary) - 32); 
            // if it's a positive number return it 
            // otherwise return the 2's complement 
            return ($binary{0} == "0" ? bindec($binary) : 
                -(pow(2, 31) - bindec(substr($binary, 1)))); 
        }


        if (rpHash($reponse) == $captcha){
            return true ;
        } else {
            return false ;
        }
    }
    //**************************************************************
    // Event Authorisation
    //**************************************************************

    /**
     * Return true if the user is Admin of the event
     * A user can be admin of an event if :
     * 1/ He is attendee + admin of the event
     * 2/ He is admin of an organization organizing an event
     * 3/ He is admin of an organization that can edit it members (canEditMembers flag) 
     *      and the organizations members is organizing the event
     * @param String $eventId The eventId to check if the userId is admin of
     * @param String $userId The userId to get the authorisation of
     * @return boolean True if the user isAdmin, False else
     */
    /* DEPRACATED BOUBOULE 05/08/2019
    public static function isEventAdmin($eventId, $userId, $attendees = null){
        $res = false;
        if(!empty($attendees) && @$attendees[$userId]["isAdmin"] == true){
            $res=true;
        }else{
            $where = array("_id"=>new MongoId($eventId),
                            "links.attendees.".$userId.".isAdmin" => true,
                            "links.attendees.".$userId.".isAdminPending" => array('$exists' => false));
            $event = PHDB::findOne(Event::COLLECTION, $where);
            if(!empty($event))
                $res=true;
        }
        
        return $res;
    }*/
    /*public static function isEventAdmin($eventId, $userId) {
        $res = false;
        $event=Event::getById($eventId);
        if(@$event["links"] && @$event["links"]["attendees"] && (@$event["preferences"]["isOpenEdtion"] && $event["preferences"]["isOpenEdtion"] !=true)){
            if(@$event["links"]["attendees"][$userId] && @$event["links"]["attendees"][$userId]["isAdmin"])
                $res=true;
            else{
                $res="openEdition";
                foreach($event["links"]["attendees"] as $value){
                    if(@$value["isAdmin"] && $value["isAdmin"]==true){
                        $res=false;
                        break;
                    }   
                }
            }
        } else {
           $res="openEdition";
        }   
        return $res;
    }*/
    //**************************************************************
    // Organization Authorisation
    //**************************************************************

    /**
     * Return true if the user is admin (not pending) of at least an organization 
     * @param String the id of the user
     * @return boolean true/false
     */
/*  FUCKING DEPRACATED BOUBOUE 05/08/2019  
    public static function isUserOrganizationAdmin($userId) {
        $res = false;
        
        //get the person links memberOf
        $personMemberOf = Person::getPersonMemberOfByPersonId($userId);

        foreach ($personMemberOf as $linkKey => $linkValue) {
            if (!empty($linkValue) && !empty($linkValue["isAdmin"])) {
                if ($linkValue["isAdmin"] && @$linkValue["isAdminPending"] != false) {
                    $res = true;
                    break;
                }
            }
        }

        return $res;
    }*/
    /**
     * Return an array with the organizations the user is admin of
     * @param String the id of the user
     * @return array of Organization (organizationId => organizationValue)
     */

    /* FUCKING DEPRACATED BOUBOUE 05/08/2019
    public static function listUserOrganizationAdmin($userId) {
        $res = array();
        $result = array();
        //organization i'am admin 
        $where = array( "links.members.".$userId.".isAdmin" => true,
                        "links.members.".$userId.".isAdminPending" => array('$exists' => false ),
                        "links.members.".$userId.".".Link::TO_BE_VALIDATED => array('$exists' => false )
                    );

        $organizations = PHDB::find(Organization::COLLECTION, $where);
        $res = $organizations;
        foreach ($organizations as $e) {
            $res[(string)new MongoId($e['_id'])] = $e;
            if (self::canEditMembersData($e['_id'])) {
                if(isset($e["links"]["members"])){
                    foreach ($e["links"]["members"] as $key => $value) {
                        if(isset($value["type"]) && $value["type"] == Organization::COLLECTION){
                            $subOrganization = Organization::getById($key);
                            $res[$key] = $subOrganization;                          
                        }
                    }
                }
            }
        }
        //$res=self::sortByName($res);
        return $res;
    }*/

    /**
     * Return true if the user is admin of the organization
     * @param String the id of the user
     * @param String the id of the organization
     * @return array of Organization (simple)
     */
    // DEPRECATED FUNCTION BOUBOULE 05/08/2019
    /*public static function isOrganizationAdmin($userId, $organizationId) {
        $res = false;
        $myOrganizations = self::listUserOrganizationAdmin($userId);
        if(!empty($myOrganizations))
          $res = array_key_exists((string)$organizationId, $myOrganizations);

        return $res;
    }*/

    // WHATTTTTTTTTTTTTTTT DEPRECATED ?? BOUBOUE 05/08/2019
    /*public static function isFormAdmin($userId, $organizationId) {
        $res = false;
        $myOrganizations = self::listUserOrganizationAdmin($userId);
        if(!empty($myOrganizations))
          $res = array_key_exists((string)$organizationId, $myOrganizations);

        return $res;
    }*/

    /**
     * Return true if the user is admin of the organization or if it's a new organization
     * @param String the id of the user
     * @param String the id of the organization
     * @return array of Organization (simple)
     */
    /*
    DEPRACATED BOUBOULE 05/08/2019
    public static function isProjectAdmin($userId, $projectId) {
        $res = false;
        $project = Project::getById($projectId);
        if( @$project["links"]['contributors'][$userid]["isAdmin"] == true )
            $res = true;
        
        return $res;
    }*/


    /* DEPREACTED BOUBOULE 05/08/2019
    public static function isOrganizationMember($userId, $organizationId) {
        $res = false;
        
        //Get the members of the organization : if there is no member then it's a new organization
        //We are in a creation process
        $organizationMembers = Organization::getMembersByOrganizationId($organizationId);
        if( array_key_exists((string)$userId, $organizationMembers) && (
            empty($organizationMembers[(string)$userId]["toBeValidated"]) || 
            $organizationMembers[(string)$userId]["toBeValidated"] == false)) 
            $res = true;    
        return $res;
    }*/
    
    /**
     * Return true if the user is member of the event
     * @param String the id of the user
     * @param String the id of the event
     * @return array of event (simple)
     */
   /* public static function isEventMember($userId, $eventId) {
        $res = false;
        
        //Get the members of the event : if there is no member then it's a new organization
        //We are in a creation process
        $eventMembers = Event::getAttendeesByEventId($eventId);
        if( array_key_exists((string)$userId, $eventMembers) && (
            empty($eventMembers[(string)$userId]["toBeValidated"]) || 
            $eventMembers[(string)$userId]["toBeValidated"] == false)) 
        $res = true;    
        return $res;
    }*/
    /**
     * Return true if the user is member of the project
     * @param String the id of the user
     * @param String the id of the project
     * @return array of Project (simple)
     */
   /* public static function isProjectMember($userId, $projectId) {
        $res = false;
        
        //Get the members of the project : if there is no member then it's a new organization
        //We are in a creation process
        $projectMembers = Project::getContributorsByProjectId($projectId);
        if( array_key_exists((string)$userId, $projectMembers) && (
            empty($projectMembers[(string)$userId]["toBeValidated"]) || 
            $projectMembers[(string)$userId]["toBeValidated"] == false)) 
        $res = true;    
        return $res;
    }*/

    /**
     * List all the event the userId is adminOf
     * A user can be admin of an event if :
     * 1/ He is attendee + admin of the event
     * 2/ He is creator of the event so admin
     * 3/ He is admin of an organization organizing an event
     * 4/ He is admin of a project organizing an event
     * @param String $userId The userId to get the authorisation of
     * @return array List of EventId (String) the user is admin of
     */
    /* DepRECATED BOUBOULE 05/08/2019
    public static function listEventsIamAdminOf($userId) {
        $eventListFinal = array();

        //event i'am admin 
        $where = array("links.attendees.".$userId.".isAdmin" => true);
        $eventListFinal = PHDB::find(Event::COLLECTION, $where);


        return $eventListFinal;
    }*/
    
/*  DepRECATED BOUBOULE 05/08/2019 
     public static function listOfEventAdmins($eventId) {
        $res = array();
        $event = Event::getById($eventId);
        if ($attendees = @$event["links"]["attendees"]){
	        foreach ($attendees as $personId => $linkDetail){
		    	if(@$linkDetail["isAdmin"]==true){
			    	array_push($res, $personId);
		    	}   
	        } 
	    }	
        return $res;
    }*/
    //**************************************************************
    // Project Authorisation
    //**************************************************************

    /**
     * Return true if the user is Admin of the project
     * A user can be admin of an project if :
     * 1/ He is attendee + admin of the project
     * 2/ He is admin of an organization organizing a project (not include)
     * 3/ He is admin of an organization that can edit it members (canEditMembers flag) (not include)
     *      and the organizations members is organizing the project
     * @param String $projectId The projectId to check if the userId is admin of
     * @param String $userId The userId to get the authorisation of
     * @return boolean True if the user isAdmin, False else
     */
    /* $isProjectAdmin = false;
	  	$admins = array();
    	if(isset($project["_id"]) && isset(Yii::app()->session["userId"])) {
    		$isProjectAdmin =  self::isProjectAdmin((String) $project["_id"],Yii::app()->session["userId"]);
    		if (!$isProjectAdmin && !empty($organizations)){
	    		foreach ($organizations as $data){
		    		$admins = Organization::getMembersByOrganizationId( (string)$data['_id'], Person::COLLECTION , "isAdmin" );
		    		foreach ($admins as $key => $member){
			    		if ($key == Yii::app()->session["userId"]){
				    		$isProjectAdmin=1;
				    		break 2;
			    		}
		    		}
	    		}
    		}
		}*/
    /*DEPRECATED BOUBOULE COMMENTED 05/08/2019
    public static function isProjectAdmin($projectId, $userId) {
    	$res = false;
    	$listProject = self::listProjectsIamAdminOf($userId);
		if( isset( $listProject[(string)$projectId] ) )
       		$res=true;
       	
       	return $res;
    }*/

    
    
	/*DEPRECATED BOUBOULE COMMENTED 05/08/2019
    public static function listProjectsIamAdminOf($userId) {
        $projectList = array();
		
        //project i'am admin 
        $where = array("links.contributors.".$userId.".isAdmin" => true,
         				"links.contributors.".$userId.".isAdminPending" => array('$exists' => false )
         		);
        $projectList = PHDB::find(Project::COLLECTION, $where);
        return $projectList;
    }*/

    //**************************************************************
    // Job Authorisation
    //**************************************************************
    // FUCKING DEPRECATED OOOOOOOOOOOOOOOLD : COMMENTED BOUBOULE 05/08/2019
    /**
     * Return true if the user is Admin of the job
     * A user can be admin of an job if :
     * 1/ He is admin of the organization posting the job offer
     * 3/ He is admin of an organization that can edit it members (canEditMembers flag) 
     *      and the organizations members is offering the job
     * @param String $jobId The jobId to check if the userId is admin of
     * @param String $userId The userId to get the authorisation of
     * @return boolean True if the user isAdmin, False else
     */
   /* public static function isJobAdmin($jobId, $userId) {
        $job = Job::getById($jobId);
        if (!empty($job["hiringOrganization"])) {
            $organizationId = (String) $job["hiringOrganization"]["_id"];
        } else {
            throw new CommunecterException("The job ". $jobId." is not well format : contact your admin.");
        }
        
        $res = self::isElementAdmin($organizationId, Organization::COLLECTION, $userId);

        return $res;
    }*/

    /**
    * Get the authorization for edit an event
    * An user can edit an event if :
    * 1/ he is admin of this event
    * 2/ he is admin of an organisation, which is the creator of an event
    * 3/ he is admin of an organisation witch can edit an organisation creator 
    * @param String $userId The userId to get the authorisation of
    * @param String $eventId event to get authorisation of
    * @return a boolean True if the user can edit and false else
    */
/*  DePreCaTED COMMENTED BY BOUBOULE 05/08/2019 
     public static function canEditEvent($userId, $eventId){
    	$res = false;
    	$event = Event::getById($eventId);
    	if(!empty($event)){

    		// case 1
    		if(isset($event["links"]["attendees"])){
    			foreach ($event["links"]["attendees"] as $key => $value) {
    				if($key ==  $userId){
	    				if(isset($value["isAdmin"]) && $value["isAdmin"]==true && empty($value["isAdminPending"])){
	    					$res = true;
	    				}
	    			}
    			}
    		}
    	}
    	return $res;
    }*/

    //**************************************************************
    // Entry Authorisation
    //**************************************************************
    
   /**
    * Get the authorization to edit an entry. The entry is stored in the survey collection.
    * A user can edit a vote if :
    * 1/ he is super admin
    * 2/ he is the organizer of the vote
    * 3/ he is admin of an organisation witch is organizer 
    * @param String $userId The userId to get the authorisation of
    * @param String $eventId event to get authorisation of
    * @return a boolean True if the user can edit and false else
    */
   


    
    /**
    * check if a user is a local citizen
    * @param cityId is a unique  city Id
    * @return a boolean
    */
    public static function isLocalCitizen($userId, $cityId) {
        $cityMap = City::getUnikeyMap($cityId);
        //echo Yii::app()->session["user"]["codeInsee"] ."==". $cityMap["insee"];
        return (@Yii::app()->session["user"]["codeInsee"] && Yii::app()->session["user"]["codeInsee"] == $cityMap["insee"] ) ? true : false;
    }

    /**
     * List the user that are admin of the organization
     * @param string $organizationId The organization Id to look for
     * @param boolean $pending : true include the pending admins. By default no.
     * @return type array of person Id
     */
    /*public static function listOrganizationAdmins($organizationId, $pending=false) {
        $res = array();
        $organization = Organization::getById($organizationId);
        
        if ($members = @$organization["links"]["members"]) {
            foreach ($members as $personId => $linkDetail) {
                if (@$linkDetail["isAdmin"] == true) {
	                $userActivated = Role::isUserActivated($personId);
	                if($userActivated){
	                    if ($pending) {
	                        array_push($res, $personId);
	                    } else if (@$linkDetail["isAdminPending"] == null || @$linkDetail["isAdminPending"] == false) {
	                        array_push($res, $personId); 
	                    }
					}
                }
            }
        }

        return $res;
    }*/

} 
?>