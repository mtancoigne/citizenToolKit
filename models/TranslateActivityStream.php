<?php 
class TranslateActivityStream {
	
	/*

	----------------- ACTIVITY STREAM ----------------- 

	https://evanprodromou.example/profile

	{
	  "@context": "http://www.w3.org/ns/activitystreams",
	  "@type": "Person",
	  "@id": "https://evanprodromou.example/profile.html#me",
	  "displayName": "Evan Prodromou",
	  "alias": "eprodrom",
	  "summary": "Founder of Fuzzy.io. Past founder of Wikitravel and StatusNet. Founding CTO of Breather.",
	  "icon": {
	    "@type": "Image",
	    "url": "https://evanprodromou.example/avatar.png",
	    "width": 128,
	    "height": 128
	  },
	  "url": {
	      "@type": "Link",
	      "href": "https://evanprodromou.example/profile.html",
	      "mediaType": "text/html"
	  },
	  "location": {
	       "@type": "Place",
	       "displayName": "Montreal, Quebec, Canada"
	  }
	}*/

		public static $dataBinding_actor =	array(
			"@context" => array("https://www.w3.org/ns/activitystreams",
							"https://w3id.org/security/v1"),
			"id"	=>  array(	"valueOf"  	=> 'id', 
															"type" 	=> "url", 
															"prefix"   => "api/activitypub/actor/id/",
															"suffix"   => "" ),
			"type"	=> "Person",

			"preferredUsername" => array("valueOf" => "username"),
			"inbox" => array("valueOf"  	=> 'username', 
															"type" 	=> "url", 
															"prefix"   => "#@",
															"suffix"   => "/inbox"),
			
			"publicKey" 	=> array("parentKey"=>"publicKey", 
	    					 "valueOf" => array(
									"id"	=>  array(	"valueOf"  	=> 'id', 
															"type" 	=> "url", 
															"prefix"   => "api/activitypub/actor/id/",
															"suffix"   => "#main-key"),
									"owner" =>  array(	"valueOf"  	=> 'id', 
															"type" 	=> "url", 
															"prefix"   => "api/activitypub/actor/id/",
															"suffix"   => ""),
									"publicKeyPem" => "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6cwwqjl4FwdeioV2dsIp\ndVxfbmcA0fgUkD834fWTlSOVjon2VxmLdEq5iLWf1yW5nYucc4iKFMoQa+8Qj7UQ\nQD1L5gnahfoK+8aCUSISs87n31N7G/cAl4ieVeT6Hiv1yL2tw/tbfO4ATKLZZMXd\niSnv6KAfAKpKsy15rXoyvMDeqNbIipqAZdRpT6/ZmPDdi56umnUVxjZP2Twm7HFg\ngBZ+zBIvnOTcBYS+D9STbKig3rMOk5/fVv4OUJSwvriiuvcegd1tixnOSbNtY9Q2\nH0wVbrbYQfPaIXI0AGktUxsEKj9AIiQZCycE5msrTazJ6smI1YWie\nahQux9v527b\nWwIDAQAB\n-----END PUBLIC KEY-----\n"))
			);

		public static $dataBinding_error = array(
			"error" => "Error is api");


		public static $dataBinding_webfinger = array(
			"subject" => array("valueOf" => "subject"),
			"links" => array("parentKey" => "links",
					"0" => array(
						"valueOf" => array(
							"rel" => array("valueOf" => "rel"),
							"type" => "application/activity+json",
							"href" => array("valueOf" => "href"))))
		);



	/*		public static $dataBinding_allActivityPub = array(
		"@context" => "http://www.w3.org/ns/activitystreams",
		"type" => array("valueOf" 	=> "verb"),
		"summary" => "Founder of Fuzzy.io. Past founder of Wikitravel and StatusNet. Founding CTO of Breather."
	);

		public static $dataBinding_create = array(
		"context" => "https://www.w3.org/ns/activitystreams",
		"type" => array("valueOf" 	=> "verb"),
		"summary" => "Founder of Fuzzy.io. Past founder of Wikitravel and StatusNet. Founding CTO of Breather.",

		"actor" 	=> array("parentKey" => 'author',
							"valueOf" => array(
								"name" => array("valueOf"	=>	"name"),
								"type" => "Person")),

		"object"	=> array("parentKey" => 'object',
							"valueOf"	=> array(
									"type"	=> array("valueOf" => "type"),
									"name"	=> array("valueOf" => "displayName"),
									"content" => array("valueOf" => "displayValue"))
						));

		public static $dataBinding_follow = array(
			"context" => "https://www.w3.org/ns/activitystreams",
			"summary" => "Test",
			"type" => array("valueOf" => "verb"),
			"actor"	=>	array("parentKey" => 'author',
							"valueOf" => array(
								"type"	=> "Person",
								"name" => array("valueOf"	=>	"name"),
								)),
			"object" => array("parentKey" => 'notify',
							"valueOf" => array(
								"name" => array("valueOf" => "{where}"),
								"type" => array("valueOf" => "{who}")))
			);*/
}

