<?php 
class ButtonCtk{
	public static function searchBar($params){
		$eventTargetInput=(!@$params["dropdownResult"]) ? "main-search-bar" : "second-search-bar";
		$eventTargetAddon=(!@$params["dropdownResult"]) ? "main-search-bar-addon" : "second-search-bar-addon";
		if(@$params["eventTargetInput"]) $eventTargetInput;
		if(@$params["eventTargetAddon"]) $eventTargetAddon;
		$classes=(isset($params["classes"])) ? $params["classes"] : "hidden-xs col-sm-4 col-md-4";
		$placeholder=(@$params["placeholder"]) ? Yii::t("common", $params["placeholder"]) : Yii::t("common", "what are you looking for ?");
		$icon=(@$params["icon"]) ? $params["icon"] : "arrow-circle-right";
		$str='<div class="'.$classes.'">'.
                '<input type="text" class="form-control pull-left text-center '.$eventTargetInput.'" id="'.$eventTargetInput.'" placeholder="'.$placeholder.'">'.
                '<span class="text-white input-group-addon pull-left '.$eventTargetAddon.'" id="'.$eventTargetAddon.'">'.
                    '<i class="fa fa-'.$icon.'"></i>'.
                '</span>';
            	//if(@$params["dropdownResult"]) 
                	$str.='<div class="dropdown-result-global-search hidden-xs col-sm-6 col-md-5 col-lg-5 no-padding" style="display:none;"></div>';  
            $str.="</div>";
        return $str;
	}
	public static function app($params){
        
		$pages=Yii::app()->session["paramsConfig"]["pages"];

        //var_dump($pages);
		//$label=(@$params["label"] && $params["label"]) ? true : false;
		//$icon=(isset($params["icon"])) ? $params["icon"] : false;
		$tooltips=(isset($params["tooltips"])) ? $params["tooltips"] : false;
		$img=(isset($params["img"])) ? $params["img"] : false;
		$nameMenuTop = (isset($params["nameMenuTop"])) ? $params["nameMenuTop"] : false ;
		$str="";
        

		foreach ($pages as $key => $value) {
            if( @$value["inMenu"]== true &&
                ( ( isset($value["inMenuTop"]) && $value["inMenuTop"] == true ) || 
                 !isset($value["inMenuTop"]) ) ){
                $labelStr=Yii::t("common", $value["subdomainName"]);

                if(!empty($value["urlExtern"])){
                    $str.= '<a href="'.$value["urlExtern"].'"'.
                                'target="_blanc" '. 
                                'class=" btn btn-link menu-button btn-menu text-dark btn-menu-tooltips">';

                /*                <i class="fa fa-<?php echo $value["icon"]; ?>"></i>
                <span class="<?php echo str_replace("#","",$key); ?>ModSpan"><?php echo Yii::t("common", $value["subdomainName"]); ?></span>
                <span class="<?php echo @$value["notif"]; ?> topbar-badge badge animated bounceIn badge-warning"></span>
            </a> */
                }else{
                    $str.='<button class="btn btn-link menu-button btn-menu lbh-menu-app text-dark btn-menu-tooltips" data-hash="'.$key.'">';
                }

                if(isset($img) && !empty($value["img"])){
                             $str.='<img width="45px" height="45px" src="'.Yii::app()->getModule("costum")->assetsUrl.$value["img"].'" class="imgMenuTop hidden-xs">'.'<h6 class="nameMenuTop hidden-xs">'.$value["nameMenuTop"].'</h6>'; 
                        }else if( !empty($value["icon"])) 
                            $str.='<i class="fa fa-'.$value["icon"].'"></i>';
                        if(!empty($value["subdomainName"])) $str.='<span>'.$labelStr.'</span>';
                        if($tooltips) $str.=self::buttonTooltips($tooltips, $labelStr);
                if(!empty($value["urlExtern"])){
                    $str.="</a>";
                }else
                    $str.="</button>";
            	
            }
        }

        //  var_dump($label);exit;
		return $str;
	}
	public static function buttonTooltips($pos, $label){
		return '<span class="tooltips-'.$pos.'-menu-btn">'.Yii::t("common", $label).'</span>';
	}
}
