<?php

class Poi {
	const COLLECTION = "poi";
	const CONTROLLER = "poi";
	const MODULE = "poi";
	const ICON = "fa-map-marker";
	
	//TODO Translate
	public static $types = array (
		//"link" 			=> "Lien, Url",
		//"tool"			=> "Outil",
		//"machine"		=> "Machine",
		//"software"		=> "Software",
		//"rh"			=> "Ressource Humaine",
		//"materialRessource" => "Ressource Materielle",
	//	"financialRessource" => "Ressource Financiere",
	//	"ficheBlanche" => "Fiche Blanche",
	//	"geoJson" 		=> "Url au format geojson ou vers une umap",
		"compostPickup" => "récolte de composte",
		"video" 		=> "video",
		"sharedLibrary" => "bibliothèque partagée",
		"artPiece" 		=> "oeuvres",
		"recoveryCenter"=> "ressourcerie",
		"trash" 		=> "poubelle",
		"history" 		=> "histoire",
		"something2See" => "chose a voir",
		//"funPlace" 		=> "endroit Sympas (skatepark, vue...)",
		//"place" 		=> "place publique",
		"streetArts" 	=> "arts de rue",
		"openScene" 	=> "scène ouverte",
		"stand" 		=> "stand",
		"parking" 		=> "Parking",
		"other"			=> "Autre",
		"cms"			=> "CMS",
		"doc"			=> "Documentation",
	);

	//From Post/Form name to database field name
	public static $dataBinding = array (
	    "section" => array("name" => "section"),
	    "type" => array("name" => "type"),
	    "subtype" => array("name" => "subtype"),
	    "category" => array("name" => "category"),
	    "name" => array("name" => "name", "rules" => array("required")),
	    "address" => array("name" => "address", "rules" => array("addressValid")),
	    "addresses" => array("name" => "addresses"),
	    "streetAddress" => array("name" => "address.streetAddress"),
	    "postalCode" => array("name" => "address.postalCode"),
	    "city" => array("name" => "address.codeInsee"),
	    "addressLocality" => array("name" => "address.addressLocality"),
	    "addressCountry" => array("name" => "address.addressCountry"),
	    "preferences" => array("name" => "preferences"),
	    "geo" => array("name" => "geo"),
	    "geoPosition" => array("name" => "geoPosition"),
	    "description" => array("name" => "description"),
	    "parent" => array("name" => "parent"),
	    "parentId" => array("name" => "parentId"),
	    "parentType" => array("name" => "parentType"),
	    "media" => array("name" => "media"),
	    "urls" => array("name" => "urls"),
	    "medias" => array("name" => "medias"),
	    "tags" => array("name" => "tags"),
	    "structags" => array("name" => "structags"),
	    "level" => array("name" => "level"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "rank" => array("name" => "rank"),
	    
	    "modified" => array("name" => "modified"),
	    "source" => array("name" => "source"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    );
//From Post/Form name to database field name
	public static $collectionsList = array (
	   "Où sont les femmes",
		"Passeur d'images",
		"MHQM",
		"MIAA",
		"Portrait citoyens",
		"Parcours d'engagement"
	);
	public static $genresList=array(
		"Documentaire",
		"Fiction",
		"Docu-fiction",
		"Films outils",
		"Films de commande"
	);
	public static function getConfig(){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            //"module"   => self::MODULE,
			//"init"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/init.js" ,
			//"form"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/dynForm.js" ,
            "categories" => CO2::getContextList(self::MODULE),
            "lbhp"=>true
		);
	}
	/**
	 * get all poi details of an element
	 * @param type $id : is the mongoId (String) of the parent
	 * @param type $type : is the type of the parent
	 * @return list of pois
	 */
	public static function getPoiByIdAndTypeOfParent($id, $type, $orderBy=null){
		$pois = PHDB::findAndSort(self::COLLECTION,array("parentId"=>$id,"parentType"=>$type), $orderBy);
	   	return $pois;
	}
	/**
	 * get poi with limit $limMin and $limMax
	 * @return list of pois
	 */
	//TODO corriger ou supprimer -> DEPRECIATED use getPoiByTagWhereSortAndLimit
	public static function getPoiByTagsAndLimit($limitMin=0, $indexStep=15, $searchByTags=array(), $where=array(), $fields=array()){
		
		if(empty($where))
			$where = array("name"=>array('$exists'=>1));
		else
			$where["name"]=array('$exists'=>1);
		if(@$searchByTags && !empty($searchByTags)){
			$queryTag = array();
			foreach ($searchByTags as $key => $tag) {
				if($tag != "")
					$queryTag[] = new MongoRegex("/".$tag."/i");
			}
			if(!empty($queryTag))
				$where["tags"] = array('$in' => $queryTag); 			
		}

		
		
		$pois = PHDB::findAndSort( self::COLLECTION, $where, array("updated" => -1), $limitMin, $fields);
	   	return $pois;
	}

	/**
	 * get poi with generic where param and limit $limMin and $limMax
	 * @return list of pois
	 */
	public static function getPoiByWhereSortAndLimit($where=array(),$orderBy=array("updated" => -1), $indexStep=10, $indexMin=0){
		$pois = PHDB::findAndSortAndLimitAndIndex(self::COLLECTION, Search::prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
		foreach($pois as $key=>$val){
			$pois[$key]["typePoi"]=@$val["type"];
			$pois[$key]["type"]=self::COLLECTION;
		}
	   	return $pois;
	}	

	//dans une liste de POI retourne tout ceux qui ont une $val donnée
	//$l is an array of pois 
	//$val is a string 
	// work for tags and structags
	public static function getPoiByStruct( $l, $val, $field="tags" ){
		$res = array();

	    foreach ( $l as $k => $v ) {
	        if( isset( $v[$field] ) && is_string($v[$field]) && $val == $v[$field] )
	        	$res[] = $v;
	        else if( isset( $v[$field] ) && is_array($v[$field]) && in_array( $val, $v[$field] ) ) 
	            $res[] = $v;
	    }
	    return $res;
	}

	/**
	 * get a Poi By Id
	 * @param String $id : is the mongoId of the poi
	 * @return poi
	 */
	public static function getById($id) { 
		
	  	$poi = PHDB::findOneById( self::COLLECTION ,$id );
	  	
	  	// Use case notragora
	  	if(@$poi["type"])
		  	$poi["typeSig"] = self::COLLECTION.".".$poi["type"];
	  	else
		  	$poi["typeSig"] = self::COLLECTION;
		if(@$poi["type"])
	  		$poi = array_merge($poi, Document::retrieveAllImagesUrl($id, self::COLLECTION, $poi["type"], $poi));
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$poi["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	$where["doctype"]="file";
	  	$poi["files"] = Document::getListDocumentsWhere($where,"file");
	  	return $poi;
	}

	public static function delete($id, $userId) {
		if ( !@$userId) {
            return array( "result" => false, "msg" => "You must be loggued to delete something" );
        }
        
        $poi = self::getById($id);
        if (!self::canDeletePoi($userId, $id, $poi)) 
        	return array( "result" => false, "msg" => "You are not authorized to delete this poi.");
        
        //Delete the comments
        $resComments = Comment::deleteAllContextComments($id,self::COLLECTION, $userId);
		if (@$resComments["result"]) {
			PHDB::remove(self::COLLECTION, array("_id"=>new MongoId($id)));
			$resDocs = Document::removeDocumentByFolder(self::COLLECTION."/".$id);
		} else {
			return $resComments;
		}
		
		return array("result" => true, "msg" => "The element has been deleted succesfully", "resDocs" => $resDocs);
	}

	public static function canDeletePoi($userId, $id, $poi = null) {
		if ($poi == null) 
			$poi = self::getById($id);
		//To Delete POI, the user should be creator or can delete the parent of the POI
        if ( $userId == @$poi['creator'] || Authorisation::canDeleteElement(@$poi["parentId"], @$poi["parentType"], $userId)) {
            return true;
        } else {
        	return false;
        }
    }

    public static function getDataBinding() {
	  	return self::$dataBinding;
	}

	//based on a list opis 
	//hierarchy can be defined with tags 
	//the parent carries tag #test, and all his children cayy #test.chichi1, #test.chichi2 , 
	//$field is the tag based field hierarchy works on...
	public static function hierarchy($pList, $field = "tags"){
		$poiList = array();
		$poiParents = array();
		foreach ($pList as $key => $value) 
		{	
			//
			if(isset( $value[ $field ]) && is_string( $value[ $field ]) ){
				$poiTree = explode(".", $value[ $field ]);
				//this is a child
				if ( count($poiTree) > 1 ) 
				{
					$parentTag = $poiTree[0];
					if(!isset($poiParents[ $parentTag ]) )
						$poiParents[ $parentTag ] = array();

					$poiParents[ $parentTag ][] = $value;
				} 
				//this is a parent
				else 
					$poiList[$value[ $field ]] = $value;
			}
			//use case if strutags is an array
			else if(isset( $value[ $field ]) && is_array( $value[ $field ]) && count($value[ $field ]))
			{
				foreach ( $value[ $field ] as $tk => $tv ) 
				{
					$poiTree = explode(".", $tv);
					if ( count($poiTree) > 1 ) 
					{
						$parentTag = $poiTree[0];
						if(!isset($poiParents[ $parentTag ]) ){
							$poiParents[ $parentTag ] = array();
						}

						$poiParents[ $parentTag ][] = $value;
					} 
					else 
						$poiList[$tv] = $value;
				}
			} else
				$poiList[] = $value; 
		}
		return array( "orphans"=>$poiList,
						
					  "parentTree"=>$poiParents );	
	}
}
?>