<?php

class Export
{ 
	public static function toCSV($data, $separ=";", $separText=" ", $notPath = false, $niv1 = true, $type=null, $order= array()) {

		$content = array();
		$allElements = "";
		$oneElement = "";
		$csv = "";
		$data = self::getMongoParam($data);

		$allPath = array();
		$ind = array();
		foreach ($data as $key1 => $value1) {
			$value1 = json_encode($value1);
			$ind[] = $key1; 
			if(substr($value1, 0,1) == "{"){
	            $allPath = ArrayHelper::getAllPathJson($value1, $allPath, $niv1);
			}
	        else{
	        	if(empty($allPath)){
	            	$allPath = array();
	        	}
	            foreach (json_decode($value1,true) as $key => $value) {

	            	if ($value != null) {
	            		$allPath = ArrayHelper::getAllPathJson(json_encode($value), $allPath, $niv1); 
	            	}
	            }
	        }
	    }
	    
		//Rest::json($allPath); exit;
		if(!empty($order)){
			$newAllPath = array();
			foreach ($order as $keyO => $valO) {
				foreach ($allPath as $keyPath => $valPath) {
					$o = explode(".", $valPath);

					if($o[0] == $valO)
						$newAllPath[] = $valPath;
				}
			}

			$allPath = $newAllPath;
			//Rest::json($allPath); exit;
		}
		
		$res = array();
		
		foreach ($allPath as $key2 => $value2) {
			foreach ($data as $key1 => $value1){
				$i = array_search($key1, $ind);
				if( empty($res[ $i ]) )
					$res[ $i ] = array();
				$valPath = ArrayHelper::getValueByDotPath($value1, $value2);
				//echo $value2." : ".$valPath."<br>";
				if ($valPath != null) {
					if(gettype($valPath) == "array"){
						array_push($res[$i],implode(";", $valPath));
					} else{
						$valPath = str_replace(";", ".", $valPath);
						$valPath = str_replace('"', "'", $valPath);
						array_push($res[$i], $valPath);
					}
				} else {
				 	array_push($res[$i], " ");
				}
			}
		}
		foreach ($res as $keyRES => $value_elt) {
			$oneElement = implode($separText.$separ.$separText, $value_elt);
			$allElements .= $separText.$oneElement.$separText;
			$allElements .= "\n";
		}
		//Rest::json($res); exit;
		// foreach ($data as $key1 => $value1) {
		// 	$value_elt = array();

		// 	foreach ($allPath as $key2 => $value2) {
		// 		if($value2 == "id" || $value2 == "_id"){
		// 			array_push($value_elt, $key1);
		// 		} else {
		// 			$valPath = ArrayHelper::getValueByDotPath($value1, $value2);
		// 			//echo $value2." : ".$valPath."<br>";
		// 			if ($valPath != null) {
		// 				if(gettype($valPath) == "array"){
		// 					array_push($value_elt,implode(";", $valPath));
		// 				} else{
		// 					$valPath = str_replace(";", ".", $valPath);
		// 					$valPath = str_replace('"', "'", $valPath);
		// 					array_push($value_elt, $valPath);
		// 				}
		// 			} else {
		// 			 	array_push($value_elt, " ");
		// 			}
		// 		}
				
		// 	}
			
		// 	// array_push($content, $value_elt);	
		// 	$oneElement = implode($separText.$separ.$separText, $value_elt);
		// 	$allElements .= $separText.$oneElement.$separText;
		// 	$allElements .= "\n";
		// }
		//Rest::json($allElements); exit;

		if( !empty(Yii::app()->session['costum'] )){
			$test = ucfirst(Yii::app()->session['costum']["slug"]);
			if(!empty($type) && $type == Project::COLLECTION &&
				!empty($test::$dataHead_allProject)){
				foreach ($allPath as $key2 => $value2) {
				
					if(!empty($test::$dataHead_allProject[$value2])){
						$allPath[$key2] = $test::$dataHead_allProject[$value2];
					}
				}
			} else if(!empty($type) && $type == Poi::COLLECTION && 
				!empty($test::$dataHead_allPoi)){
				foreach ($allPath as $key2 => $value2) {
				
					if(!empty($test::$dataHead_allPoi[$value2])){
						$allPath[$key2] = $test::$dataHead_allPoi[$value2];
					}
				}
			}
		}
		

		$head = implode($separText.$separ.$separText, $allPath);
		$csv = $separText.$head.$separText;
		$csv .= "\n";
		$csv .= $allElements;
		//Rest::json($csv); exit;
		echo $csv; 
}


	public static function getMemberOf($id, $type) {
		$data = Element::getByTypeAndId($type, $id);

		$list_orga = array();

		foreach ($data["links"]["memberOf"] as $key => $value) {
			$orga = Element::getByTypeAndId($value['type'], $key );
			$list_orga[] = $orga;
		}
		return $list_orga;
	}

	public static function getMongoParam($data) {

		foreach ($data as $key => $value) {

			if (isset($value["_id"])) {
				$data[$key]['_id'] = (string)$value["_id"];
			}

			if (isset($value['modified'])) {
				$data[$key]['modified'] = (string)$value['modified'];
			}

			if (isset($value['badges'])) {
				$data[$key]['badges'][0]['date'] = (string)$value['badges'][0]['date'];
			}

		}

		return $data;
	}
}

