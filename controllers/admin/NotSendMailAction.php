<?php
class NotSendMailAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $r = PHDB::find(ActivityStream::COLLECTION, array("verb" => ActStr::VERB_NOSENDING));
        foreach ($r as $key => $value) {
        	$r[$key]["date"] = date(DateTime::ISO8601, $value["created"]->sec);
        }

        $params = array("results" => $r);
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("notSendMail",$params,true);
        else 
            $controller->render("notSendMail",$params);
    }
}

?>