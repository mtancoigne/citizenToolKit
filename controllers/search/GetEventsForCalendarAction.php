<?php
class GetEventsForCalendarAction extends CAction{

    public function run($startDate , $endDate){
        $community = !empty($_POST['community']) ? $_POST['community'] : null;
        $sourceKey = !empty($_POST['sourceKey']) ? $_POST['sourceKey'] : "";
        $searchLocality = isset($_POST['locality']) ? $_POST['locality'] : null;
        $searchGeo = !empty($_POST["geoSearch"]) ? $_POST["geoSearch"] : null;

        
        $query = array();
        $query = Search::searchString("", $query);

        if(!empty(Yii::app()->session["costum"]["slug"])){
            $query = array('$and' => 
                            array(  $query , 
                                    array("source.toBeValidated.".Yii::app()->session["costum"]["slug"] => array('$exists'=>false) )
                            ) );
            //$query = Search::searchSourceKey(Yii::app()->session["costum"]["slug"], $query);
        }

        if(!empty($searchLocality) || !empty($searchGeo)){
            $query = Search::searchLocality($searchLocality, $query, $searchGeo);
        }
        
        $query = Search::getQueryEvents($query, null, $startDate, $endDate);
        

        
        $sort=array("updated" => -1);
        //$sort= array();

        $query = array('$and' => 
                    array( $query , 
                        array("state" => array('$nin' => array("uncomplete", "deleted")),
                            "status" => array('$nin' => array("uncomplete", "deleted", "deletePending")),
                            '$or'=>array(
                                    array('preferences.private'=>array('$exists'=>false)), 
                                    array('preferences.private'=>false),
                                 )
                        )   
                    )
                );
       if ($community != false) {
            $queryCommunity = Search::searchEventsCommunity($query,$community);
            // Rest::json($queryCommunity); exit;
        }

        $query = Search::searchSourceKey($sourceKey, $query);
        if(!empty($queryCommunity)){
            $query=array(
                '$or' =>array(
                    $query,
                    $queryCommunity
                )
            );
        }

        //Rest::json($query); exit;
        $allEvents = PHDB::findAndSortAndLimitAndIndex( PHType::TYPE_EVENTS, $query, 
                                        $sort , 0, 0);
        foreach ($allEvents as $key => $value) {
            $allEvents[$key]["typeEvent"] = @$allEvents[$key]["type"];
            $allEvents[$key]["type"] = "events";
            $allEvents[$key]["typeSig"] = Event::COLLECTION;
            if(@$value["links"]["attendees"][Yii::app()->session["userId"]]){
                $allEvents[$key]["isFollowed"] = true;
            }
            if(@$allEvents[$key]["startDate"] && @$allEvents[$key]["startDate"]->sec){
                $allEvents[$key]["startDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
                $allEvents[$key]["startDate"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
            }
            if(@$allEvents[$key]["endDate"] && @$allEvents[$key]["endDate"]->sec){
                $allEvents[$key]["endDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
                $allEvents[$key]["endDate"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
            }
            if(!empty($value["organizer"])){
                foreach($value["organizer"] as $k => $v){ 
                    $elt=Element::getElementSimpleById($k, $v["type"],null, array("slug", "profilThumbImageUrl", "name"));
                    if(!empty($elt)){
                        $allEvents[$key]["organizer"][$k]["name"] = $elt["name"];
                        $allEvents[$key]["organizer"][$k]["profilThumbImageUrl"] = ( !empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "" ) ;
                        $allEvents[$key]["organizer"][$k]["slug"] = $elt["slug"];
                    }
                }
            }
            $el = $value;
            if(@$el["links"]) 
            foreach(array("attendees") as $k) 
                if(@$value["links"][$k])
                $allEvents[$key]["counts"][$k] = count(@$value["links"][$k]);
        }
        $res = array("events" => $allEvents,
                        "count" => count($allEvents));
        
        Rest::json($res);

        Yii::app()->end();
    }
}