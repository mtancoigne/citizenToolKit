<?php
class SearchNextEventAction extends CAction{

    public function run(){
        
        $query = array();
        $query = Search::searchString("", $query);

        if(!empty(Yii::app()->session["costum"]["slug"])){
            $query = array('$and' => 
                            array(  $query , 
                                    array("source.toBeValidated.".Yii::app()->session["costum"]["slug"] => array('$exists'=>false) )
                            ) );
            $query = Search::searchSourceKey(Yii::app()->session["costum"]["slug"], $query);
        }
        
        if( !empty($_POST["searchTags"]) ){
            $queryTags =  Search::searchTags($_POST["searchTags"], '$in') ;
            if(!empty($queryTags))
                $query = array('$and' => array( $query , $queryTags) );
        }
        
        if(!empty($_POST["startDateUTC"])){
            $date1 = new DateTime($_POST["startDateUTC"]);
            $startD = $date1->getTimestamp();
            $queryDate=array(
                '$or' =>array(
                    array("startDate" => array('$exists' => 0)),
                    array(
                        "startDate" => array(   '$gte' => new MongoDate((float)$startD) 
                    ))
                    // ,
                    // array(
                    //     "startDate" => array( '$lte' => new MongoDate( (float)$startD )), 
                    //     "endDate" => array( '$gte' => new MongoDate( (float)$startD ))
                    // )
                )
            );
            $query=array(
                '$and' =>array(
                    $query,
                    $queryDate,
                    array("startDate" => array('$exists' => 1)),
                )
            );
        }
        
        $sort=array("startDate" => 1);
        //$sort= array();

        $query = array('$and' => 
                    array( $query , 
                        array("state" => array('$nin' => array("uncomplete", "deleted")),
                            "status" => array('$nin' => array("uncomplete", "deleted", "deletePending")),
                            '$or'=>array(
                                    array('preferences.private'=>array('$exists'=>false)), 
                                    array('preferences.private'=>false),
                                 )
                        )
                    )
                );
        $allEvents = PHDB::findAndSortAndLimitAndIndex( Event::COLLECTION, $query, $sort , 1, 0);
       
        Rest::json($allEvents);

        Yii::app()->end();
    }
}