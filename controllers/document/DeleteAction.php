<?php
class DeleteAction extends CAction {
	

	public function run($id=null, $contextType=null,$contextId=null) {
		if (! Person::logguedAndValid()) {
			echo json_encode(array('result'=>false,'error'=>Yii::t("common","Please Log in order to update document !")));
			return;
		}

		if (isset($_POST["path"]) && $_POST["path"]=="communevent"){
			// Method for Communevent
			Document::removeDocumentCommuneventByObjId($_POST["docId"], Yii::app()->session["userId"]);
			if(@$_POST["source"] && $_POST["source"]=="gallery")
				News::removeNewsByImageId($_POST["docId"]);
			echo json_encode(array('result'=>true, "msg" => Yii::t("document","Image deleted")));
		} else {
			if(@$_POST["ids"]){
				if(Authorisation::canEditItem(Yii::app()->session["userId"], $contextType, $contextId)){
					foreach($_POST["ids"] as $data){
						Document::removeDocumentById($data, true);
					}
				}else
					echo json_encode(array('result'=>false,'error'=>Yii::t("common","You are not allowed to delete this documents")));
			}else if(!empty($id)){
				Document::removeDocumentById($id);
			}
			echo json_encode(array('result'=>true, "msg" => Yii::t("document","Image deleted")));
	    }
	}
}