<?php
class UploadSaveAction extends CAction {
	
    //$folder is the $type of the element
	public function run($dir,$folder=null,$ownerId=null,$input, $contentKey=false, $docType=false, $rename=false, $folderId=null, $surveyId=null, $subDir=null) {
		
        $res = array('result'=>false, 'msg'=>Yii::t("document","Something went wrong with your upload!"));
        if (Person::logguedAndValid()) 
        {
            if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
                $res = array('result'=>false,'error'=>Yii::t("document","Error! Wrong HTTP method!"));
            }
            //{"result":false,"msg":"Le chargement du document ne s'est pas deroul\u00e9 correctement",
            //"file":{ "qqfile":{"name":"compo.jpg","type":"","tmp_name":"","error":6,"size":0}}}
            if(array_key_exists($input,$_FILES) && $_FILES[$input]['error'] == 0 ) {
                $file = $_FILES[$input];
            } else {
                error_log("WATCH OUT ! - ERROR WHEN UPLOADING A FILE ! CHECK IF IT'S NOT AN ATTACK");
                $res = array('result'=>false,'msg'=>Yii::t("document","Something went wrong with your upload!"));
            }
            $res['file'] = @$file;   
            $pathArray=array(
                "dir"=>$dir,
                "typeEltFolder"=>$folder,
                "idEltFolder"=>$ownerId,
                "contentKey"=>@$contentKey,
                "docType"=>@$docType,
                "folderId"=>@$folderId,
                "subDir"=>@$subDir
            );
            if(isset($_POST["restricted"]))
                $pathArray["restricted"]=true;
            if(isset($_POST["qqfilename"]))
                $pathArray["nameUrl"]=$_POST["qqfilename"];
            $res = Document::checkFileRequirements($file, $input, $pathArray);
            if ($res["result"]) {
                $nameUrl=(isset($pathArray["nameUrl"])) ? $pathArray["nameUrl"] : null;
                $res = Document::uploadDocument($file, $res["uploadDir"],$input,$rename, $nameUrl, null);
                if ($res["result"]) {
                    $res = array('resultUpload'=>true,
                                "success"=>true,
                                'name'=>$res["name"],
                                //'dir'=> $res["uploadDir"],
                                "docPath"=>$res["uploadDir"].$res["name"],
                                'size'=> (int)filesize ($res["uploadDir"].$res["name"]) );
                }
            }
            $res2 = array();
            
            if( $res["resultUpload"] ){
            //error_log("resultUpload xxxxxxxxxxxxxxxx");
                /*if($contentKey==false){
                    if(@$_POST["contentKey"]) $contentKey=$_POST["contentKey"];
                    else $contentKey=Document::IMG_PROFIL;
                }*/
                $subFolder="";
                if(@$_POST["formOrigin"])
                    $subFolder="/".$_POST["formOrigin"];
                if($contentKey==Document::IMG_SLIDER)
                    $subFolder="/".Document::GENERATED_ALBUM_FOLDER;
                if(@$docType && $docType==Document::DOC_TYPE_FILE){
                    $subFolder="/".Document::GENERATED_FILE_FOLDER;
                    if(@$subDir)
                        $subFolder.="/".str_replace(".","/", $subDir);
                }
                $folderPath=$folder."/".$ownerId;
                if(isset($_POST["restricted"])) $folderPath.="/restricted";
                $folderPath.=$subFolder;
                $params = array(
                    "id" => $ownerId,
                    "type" => $folder,
                    "folder" => $folderPath,
                    "moduleId" => "communecter",
                    "name" => $res["name"],
                    "size" => (int) $res['size'],
                    "contentKey" => $contentKey,
                    "doctype"=> $docType,
                    "author" => Yii::app()->session["userId"],
                );
                if(isset($_POST["subKey"]))
                    $params["subKey"]=$_POST["subKey"];
                if(isset($_POST["cryptage"]))
                    $params["cryptage"]=$_POST["cryptage"];
                if(isset($_POST["restricted"]))
                    $params["restricted"]=$_POST["restricted"];
                if(isset($surveyId))
                    $params["surveyId"]=$surveyId;
                if(isset($folderId))
                    $params["folderId"]=$folderId;
                if(isset($_POST["parentType"]))
                    $params["parentType"] = $folder;
                if(isset($_POST["parentId"]))
                    $params["parentId"] = $ownerId;           
                if(isset($_POST["formOrigin"]))
                    $params["formOrigin"] = $_POST["formOrigin"];
                if(isset($_POST["cropX"]))
                    $params["crop"]=array("cropX" => $_POST["cropX"],"cropY" => $_POST["cropY"],"cropW" => $_POST["cropW"],"cropH" => $_POST["cropH"]);
                
                $res2 = Document::save($params);
                if(@$res2["survey"] && $res2["survey"]){
                    $res2["survey"]=substr($subDir, strrpos($subDir, '.') + 1);
                }
            }

        } else 
            $res2 = array("result" => false, "msg" => Yii::t("common","Please Log in order to update document !"));
            
        
        $res = array_merge($res,$res2 );
        return Rest::json($res);
	}

}