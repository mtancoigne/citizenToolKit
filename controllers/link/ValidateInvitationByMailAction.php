<?php
/**
 * Action used to validate a link between a child and a parent
 * Ex : 
 * - member need to be validate by an admin of the organization/projet
 * - new admin demand must be validated by a current admin of the organization/projet
 * 
 */
class ValidateInvitationByMailAction extends CAction {
    public function run($userId, $targetType, $targetId, $answer, $redirect=null, $costum=null ) {
		// assert('!empty($_POST["childId"]); //The child id is mandatory');
		// assert('!empty($_POST["childType"]); //The child type is mandatory');
	 //    assert('!empty($_POST["parentId"]); //The parent id is mandatory');
	 //    assert('!empty($_POST["parentType"]); //The parent type is mandatory');
	 //    assert('!empty($_POST["linkOption"]); //The link option is mandatory');


    	//Rest::json($answer); exit;
    	$controller=$this->getController();
		$res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
		$user = PHDB::findOneById(Person::COLLECTION, $userId, array("roles"));
		$target = PHDB::findOneById($targetType,$targetId, array("links"));
		

		if( !empty($user) && 
			!empty($target["links"]) && 
			!empty($target["links"][Link::$linksTypes[$targetType][Person::COLLECTION]]) && 
			!empty($target["links"][Link::$linksTypes[$targetType][Person::COLLECTION]][$userId]) && 
			!empty($target["links"][Link::$linksTypes[$targetType][Person::COLLECTION]][$userId][Link::IS_INVITING])){

			$urlValidation = "";
			$urlElt = "#page.type.".$targetType.".id.".$targetId ;
			if( !empty($user["roles"]) &&
				!empty($user["roles"]["tobeactivated"]) && 
				$user["roles"]["tobeactivated"] == true ){
				$validationKey =Person::getValidationKeyCheck($userId);
				$urlElt = "/person/validateinvitation/user/".$userId.'/validationKey/'.$validationKey.'/invitation/1/';
			}

			if($answer ==  "true")
				$res = Link::validateLink($targetId, $targetType, $userId, Person::COLLECTION, Link::IS_INVITING, $userId);
			else if($answer ==  "false"){
				$res = Link::disconnect($userId, Person::COLLECTION, $targetId, $targetType, $userId, Link::$linksTypes[Person::COLLECTION][$targetType]);
				$res = Link::disconnect($targetId, $targetType, $userId, Person::COLLECTION, $userId, Link::$linksTypes[$targetType][Person::COLLECTION]);
			}
			
			$urlRedirect=Yii::app()->createUrl("/".$controller->module->id).$urlValidation.$urlElt;
			//var_dump($urlRedirect ); exit ;
	        if(@$redirect){
	            if(strrpos($redirect, "survey") !== false || strrpos($redirect, "costum") !== false) {
	                $redirect=str_replace(".", "/", $redirect);
	                $urlRedirect=Yii::app()->createUrl($redirect.$urlValidation.$urlElt);
	            }
	        } else if(isset($costum) && $costum)
	            $controller->redirect(Yii::app()->createAbsoluteUrl($urlValidation.$urlElt) );
	        $controller->redirect($urlRedirect);
		} else {
			//var_dump("here" ); exit ;
			$controller->redirect(Yii::app()->createUrl(""));
			// $controller->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
		}

		return Rest::json($res);
	}
}