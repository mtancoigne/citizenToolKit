<?php
class UpdateAdminLinkAction extends CAction{
	public function run(){
		//Rest::json($_POST); exit ;
		$form = PHDB::findOne( $_POST["parentType"] , 
								array("_id"=> new MongoId($_POST["parentId"]) ), 
								array("links" ) );

		//var_dump($_POST["$form "]);
		if(!empty($form) && 
			!empty($form["links"]) && 
			!empty($form["links"][Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]]][$_POST["childId"]])){
			if($_POST["isAdmin"] == "true"){
				$form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdmin"] = true ;
			} else {
				unset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdmin"]);
			}
			//var_dump("update parentType");
			$res = PHDB::update( $_POST["parentType"], 
										array("_id" => new MongoId($_POST["parentId"])), 
										array('$set' => array("links" => $form["links"])));
		}

		$formChild = PHDB::findOne( $_POST["childType"] , 
								array("_id"=> new MongoId($_POST["childId"]) ), 
								array("links" ) );
		//var_dump($formChild);
		if(!empty($formChild) && 
			!empty($formChild["links"]) && 
			!empty($formChild["links"][Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]]][$_POST["parentId"]])){
			if($_POST["isAdmin"] == "true"){
				$formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdmin"] = true ;
			} else {
				unset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdmin"]);
			}
			//var_dump("update childType");
			$res = PHDB::update( $_POST["childType"], 
										array("_id" => new MongoId($_POST["childId"])), 
										array('$set' => array("links" => $formChild["links"])));
		}

		Rest::json($form); exit ;
	}
}