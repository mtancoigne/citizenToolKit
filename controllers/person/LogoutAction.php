<?php
class LogoutAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        Person::clearUserSessionData();
        $url = "/#";
        if(@$_GET["network"])
        	$url="/network/default/index?src=".$_GET["network"];
        if(isset(Yii::app()->session['costum']) && !empty(Yii::app()->session['costum']))
        	$url=Yii::app()->session['costum']["url"];
        $url=Yii::app()->createUrl($url);
        
        if(isset(Yii::app()->session['costum']) && isset(Yii::app()->session['costum']["host"]))
            $controller->redirect(Yii::app()->createAbsoluteUrl(""));
        else 
            $controller->redirect($url);
    }
}