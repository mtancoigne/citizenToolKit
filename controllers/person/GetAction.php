<?php
class GetAction extends CAction {

	public function run($id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null) {
		$controller=$this->getController();
		// Get format

		if( !empty(Yii::app()->session['costum'] )){
			$test = ucfirst(Yii::app()->session['costum']["slug"]);
			if(!empty($test::$dataBinding_allPerson))
				$bindMap = $test::$dataBinding_allPerson;


		}

		if( empty($bindMap) && $format == Translate::FORMAT_SCHEMA)
			$bindMap = (empty($id) ? TranslateSchema::$dataBinding_allPerson : TranslateSchema::$dataBinding_person);
		else if(empty($bindMap) && $format == Translate::FORMAT_PLP )
			$bindMap = TranslatePlp::$dataBinding_person;
		else if(empty($bindMap) && $format == Translate::FORMAT_AS )
			$bindMap = TranslateActivityStream::$dataBinding_person;
		else if(empty($bindMap) && $format == Translate::FORMAT_KML)
			$bindMap = (empty($id) ? TranslateKml::$dataBinding_allPerson : TranslateKml::$dataBinding_person);
		else if(empty($bindMap) && $format == Translate::FORMAT_GEOJSON)
			$bindMap = (empty($id) ? TranslateGeoJson::$dataBinding_allPerson : TranslateGeoJson::$dataBinding_person);
		else if(empty($bindMap) &&
				($format == Translate::FORMAT_MD || 
				 $format == Translate::FORMAT_TREE || 
				 $format == Translate::FORMAT_FINDER ) )
			$bindMap = Person::CONTROLLER;
		else if (empty($bindMap) && $format == Translate::FORMAT_JSONFEED )
			$bindMap = TranslateJsonFeed::$dataBinding_allPerson;
		else if (empty($bindMap) && $format == "valueflows")
              $bindMap = TranslateValueFlows::$dataBinding_agent;
		else if( empty($bindMap) ){
			$bindMap = ( empty($id) ? TranslateCommunecter::$dataBinding_allPerson : TranslateCommunecter::$dataBinding_person);
		}

		$result = Api::getData($bindMap, $format, Person::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee);		
		
		if ($format == Translate::FORMAT_KML) {
			$strucKml = News::getStrucKml();    
			Rest::xml($result, $strucKml,$format);
		} 
		else if ($format == Translate::FORMAT_MD) {
			//header('Content-Type: text/markdown');
			echo $result;
		} else if ($format == "csv") {
			// $res = $result["entities"];
			// $head = Export::toCSV($res, ";", "'");
			//Rest::json($result["entities"]); exit;
			Rest::csv($result["entities"]);
		} 
		else
			Rest::json($result);

		Yii::app()->end();
	}
}