<?php

class MailsListAction extends CAction
{
    public function run() {
        $controller = $this->getController();
    	$params = array();
        $where=array();
        if(isset(Yii::app()->session["costum"]) && isset(Yii::app()->session["costum"]["slug"]))
            $where=array("source.key"=>Yii::app()->session["costum"]["slug"]);
    	$params["results"] = Cron::getCron($where);
    	//$params["city"] = json_encode($city) ;
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("mailslist",$params,true);
        else 
            $controller->render("mailslist",$params);
    }
}

?>