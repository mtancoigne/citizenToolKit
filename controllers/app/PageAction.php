<?php

class PageAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($type, $id, $view=null, $mode=null, $dir=null, $key=null, $folder=null) { 
    	$controller=$this->getController();
        $this->getController()->layout = "//layouts/mainSearch";
        
        CO2Stat::incNbLoad("co2-page");
            
        if( $type == Person::COLLECTION  || $type == Event::COLLECTION || 
            $type == Project::COLLECTION || $type == Organization::COLLECTION || 
            $type == Poi::COLLECTION || $type == Place::COLLECTION || $type == Ressource::COLLECTION || 
            $type == Classified::COLLECTION){
            $element = Element::getByTypeAndId($type, $id);
        }
        else if($type == News::COLLECTION){
            $element = News::getById($id);
        }

        else if($type == Product::COLLECTION){
            $element = Product::getById($id);
        }
        else if($type == Service::COLLECTION){
            $element = Service::getById($id);
        }
        else if($type == Survey::COLLECTION){
            $element = Survey::getById($id);
        }
        if(isset($controller)){
            $shortDesc =  @$element["shortDescription"] ? $element["shortDescription"] : "";
            if($shortDesc=="")
                $shortDesc = @$element["description"] ? substr($element["description"], 0, 140)."..." : "";
            $controller->module->description = $shortDesc;
            $controller->module->pageTitle = (isset($element["name"])) ? $element["name"] : "";
            $controller->module->author = "";//(isset($element["metaTitle"])) ? $element["metaTitle"] : @$element["title"];
            $controller->module->keywords = (@$element["tags"]) ? implode(",", @$element["tags"]) : "";
            if (@$element["profilImageUrl"]) {
                $controller->module->image = $element["profilImageUrl"];
            }
            //var_dump($_SERVER["REMOTE_ADDR"]);var_dump(Yii::app()->params["serverIp"]);exit;
            $controller->module->relCanonical = Yii::app()->createUrl("/co2/app/page/type/".$type."/id/".$id);
            if( strstr($_SERVER['REQUEST_URI'], "app/page") ){
                // referer not from the same domain
                $controller->module->share=Yii::app()->createUrl("#page.type.".$type.".id.".$id);
                if(!Yii::app()->request->isAjaxRequest){
                    $controller->layout = "//layouts/meta";
                    $controller->render( "welcome" , array());
                }
            }
            
        }
        //element deleted 
        if( (!empty($element["status"]) && $element["status"] == "deleted") ||  
            (!empty($element["tobeactivated"]) && $element["tobeactivated"] == true) ){
              $controller->redirect( Yii::app()->createUrl($controller->module->id) );
            }

        //visibility authoraizations
        if(!Preference::isPublicElement(@$element["preferences"]) &&
             (!@Yii::app()->session["userId"] || !Authorisation::canSeePrivateElement(@$element["links"], $type, $id, $element["creator"], @$element["parentType"], @$element["parentId"]))){
                    $url = "/#";
                if(@$_GET["network"])
                    $url="/network/default/index?src=".$_GET["network"];
                if(isset(Yii::app()->session['costum']) && !empty(Yii::app()->session['costum']))
                    $url=Yii::app()->session['costum']["url"];
                $url=Yii::app()->createUrl($url);
                
                if(isset(Yii::app()->session['costum']) && isset(Yii::app()->session['costum']["host"]))
                    $controller->redirect(Yii::app()->createAbsoluteUrl(""));
                else 
                    $controller->redirect($url);
             //$controller->redirect( Yii::app()->createUrl("") );
        }

        if(@$element["parentId"] && @$element["parentType"] && 
            $element["parentId"] != "dontKnow" && $element["parentType"] != "dontKnow" && !@$element["parent"]){
            $element['parent'][$element["parentId"]] = Element::getElementById( $element["parentId"], $element["parentType"], null, array("name", "slug","profilThumbImageUrl"));
            $element['parent'][$element["parentId"]]["type"] = $element["parentType"];
        }
            
        if(@$element["parent"] && !empty($element["parent"]) && !@$element["parent"]["name"]){
            foreach($element["parent"] as $k => $v){
                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                if(!empty($elt))
                    $element['parent'][$k]=array_merge($element['parent'][$k], $elt);
            }
        }
        
        if(@$element["organizerId"] && @$element["organizerType"] && 
            $element["organizerId"] != "dontKnow" && $element["organizerType"] != "dontKnow"){

            $element['organizer'][$element["organizerId"]] = Element::getElementById( $element["organizerId"], $element["organizerType"], null, array("name", "slug","profilThumbImageUrl"));
            $element['organizer'][$element["organizerId"]]["type"] = $element["organizerType"];
        }
            
        if(@$element["organizer"] && !empty($element["organizer"])){
            foreach($element["organizer"] as $k => $v){
                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                if(!empty($elt))
                    $element['organizer'][$k]=array_merge($element['organizer'][$k], $elt);
            }
        }
        $params = array("id" => @$id,
                        "type" => @$type,
                        "view" => @$view,
                        "dir" => @$dir,
                        "key" => @$key,
                        "folder" => @$folder,
                        "subdomain" => "page",
                        "mainTitle" => "Page perso",
                        "placeholderMainSearch" => "",
                        "element" => @$element,
                        "canEdit"=> false,
                        "canParticipate"=>false,
                        "canSee"=>true);

       if(Authorisation::canEditItem(Yii::app()->session["userId"], $type ,$id)){
            $params["canEdit"]=true;
            $params["canParticipate"]=true;
            $params["canSee"]=true;
        }else if(Authorisation::canParticipate(Yii::app()->session["userId"], $type ,$id)){
            $params["canParticipate"]=true;
            $params["canSee"]=true;
        }else if(Authorisation::canSee($type ,$id)){
            $params["canSee"]=true;
        }
        $params = Element::getInfoDetail($params, $element, $type, $id);
        $params["typeItem"] = ( !empty($element["typeSig"]) && $element["typeSig"] != "" ) ? $element["typeSig"] : "" ;
        if($params["typeItem"] == "") $params["typeItem"] = @$element["type"] ? $element["type"] : "item";
        if($params["typeItem"] == "people") $params["typeItem"] = Person::COLLECTION ;
        $params["typeItemHead"] = $params["typeItem"];
        if($params["typeItem"] == Organization::COLLECTION && @$element["type"]) $params["typeItemHead"] = $element["type"];
        $params["categoryItem"]=(isset($element["category"]) && is_string($element["category"])) ? $element["category"] : "";
        //icon et couleur de l'element
        $params["icon"] = Element::getFaIcon($params["typeItemHead"]) ? Element::getFaIcon($params["typeItemHead"]) : "";
        $params["iconColor"] = Element::getColorIcon($params["typeItemHead"]) ? Element::getColorIcon($params["typeItemHead"]) : "";
        $params["useBorderElement"] = false;
        $params["pageConfig"]=(@Yii::app()->session['paramsConfig']["element"]) ? Yii::app()->session['paramsConfig']["element"] : null;
        $params["addConfig"]=(@Yii::app()->session['paramsConfig']["add"]) ? Yii::app()->session['paramsConfig']["add"] : null;


        //bloque l'édition de la page (même si on est l'admin)
        //visualisation utilisateur
        if(@$mode=="noedit"){ $params["edit"] = false; }
        
        if(@$_POST["preview"] == true){

            $params["preview"]=$_POST["preview"]; 
            $typePreview=(in_array($type,[Organization::COLLECTION, Person::COLLECTION, Project::COLLECTION, Event::COLLECTION])) ? "element" : $type;
            if( isset(Yii::app()->session['costum']["htmlConstruct"]) 
                && isset(Yii::app()->session['costum']["htmlConstruct"]["preview"])
                && isset(Yii::app()->session['costum']["htmlConstruct"]["preview"][$typePreview])){
                echo $controller->renderPartial(Yii::app()->session['costum']["htmlConstruct"]["preview"][$typePreview], $params);
            }else{
                if($type == "classifieds") echo $controller->renderPartial('eco.views.co.preview', $params );
                else if($type == "poi") echo $controller->renderPartial('../poi/preview', $params ); 
                else echo $controller->renderPartial("../element/preview", $params);
            }
        } 
        else{
            if(Yii::app()->request->isAjaxRequest)
                echo $controller->renderPartial("page", $params, true);
            else 
                $controller->render( "page" , $params);
        }
    }
}