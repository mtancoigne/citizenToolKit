<?php

class AdminAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($view=null, $dir=null, $subview=null, $contextId=null, $contextType=null){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-admin");   
        $params = array(
            "dir" => @$dir,
            "view"=>@$view,
            "subview"=>@$subview,
            "authorizedAdmin"=>false,
            "addButton"=>true,
            "menu"=>true
        );
        if(isset($_POST["subView"]) && !empty(true)){
            $params["subView"]=$_POST["subView"];
        }
        $view = "index";
        $redirect="";
        if(Authorisation::isInterfaceAdmin())
            $params["authorizedAdmin"]=true;
        else if(Costum::sameFunction("authorizedPanelAdmin"))
            $params["authorizedAdmin"]=true;
        else if(!empty($contextId) && !empty($contextType))
            $params["authorizedAdmin"]=Authorisation::isElementMember($contextId, $contextType ,  Yii::app()->session["userId"]);
        if(isset($_POST["options"])){
            foreach($_POST["options"] as $k => $v){
                $params[$k]=$v;   
            } 
        }
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("../admin/".$redirect.$view, $params, true);
        else
            $controller->render("../admin/".$redirect.$view, $params);
    }
}