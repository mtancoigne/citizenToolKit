<?php

class IndexAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-welcome");  
        
        $hash = (@Yii::app()->session["userId"]) ? Yii::app()->session['paramsConfig']["pages"]["#app.index"]["redirect"]["logged"] : Yii::app()->session['paramsConfig']["pages"]["#app.index"]["redirect"]["unlogged"];
        $params = array("type" => @$type );
        if(!@$hash || @$hash==""){ 
            $hash="search";
        }
        else if($hash=="agenda"){
            $hash="search";
            $params = array("type"=>"events");
        }
        //var_dump($hash);exit;
        if(Yii::app()->request->isAjaxRequest){
            echo $controller->renderPartial($hash, $params, true);
        }
        else 
            $controller->render( $hash , $params);
    }
}