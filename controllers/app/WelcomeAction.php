<?php

class WelcomeAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-welcome");  
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("welcome", array(), true);
        else 
            $controller->render( "welcome" , array());
    }
}