<?php

class SearchAction extends CAction
{
    public function run($q  = null, $id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $fullRepresentation = "true") {
        //$controller=$this->getController();
        
		$result = Tags::searchActiveTags($q);
		Rest::json($result);

		Yii::app()->end();
    }
}
