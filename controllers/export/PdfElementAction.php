<?php
class PdfElementAction extends CTKAction{
	public function run($id, $type){

		$controller=$this->getController();
		ini_set('max_execution_time',1000);
		
		$paramsCostum["controller"] = $controller;
		$paramsCostum["slug"] = null; 
		$paramsCostum["admin"] = null; 
		$paramsCostum["id"] = null; 
		$paramsCostum["idElt"] = $id;

		

		if(Costum::isSameFunction("pdfElement")){
			if(isset($_POST["save"])){
				$res=Document::checkFileRequirements([], null, 
					array(
						"dir"=>"communecter",
						"typeEltFolder"=> Form::ANSWER_COLLECTION,
						"idEltFolder"=> $id,
						"docType"=> "file",
						"nameUrl"=>"/pdf.pdf",
						"sizeUrl"=>1000
					) ) ;
				$paramsCostum["comment"]=true;
				$paramsCostum["saveOption"]="F";
				$paramsCostum["urlPath"]=$res["uploadDir"];
				// $paramsCostum["docName"]=$_POST["title"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
				//  if(isset($_POST["subKey"]))
				$paramsCostum["docName"]=$_POST["subKey"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
			}

			
			$params = Costum::sameFunction("pdfElement", $paramsCostum);

			if(isset($_POST["save"])){	
				$kparams = [
	                "id" => $id,
	                "type" => Form::ANSWER_COLLECTION,
	                "folder" => Form::ANSWER_COLLECTION."/".$id."/".Document::GENERATED_FILE_FOLDER,
	                "moduleId" => "communecter",
	                "name" => $paramsCostum["docName"],
	                "size" => "",
	                "contentKey" => "",
	                "doctype"=> "file",
	                "author" => Yii::app()->session["userId"]
	            ];
	            if(isset($_POST["subKey"]))
					$kparams["subKey"]=$_POST["subKey"];

		        $res2 = Document::save($kparams);
		        return Rest::json( [ "res"=>true, "docPath"=>"/upload/".$kparams["moduleId"]."/".$kparams["folder"]."/".$params["docName"], "fileName"=>$params["docName"] ] );
		    }
		} 
		else 
		{
			$elt = PHDB::findOneById( $type , $id);
			$params = [ "elt" => $elt, "type" => $type , "id" => $id ];
			//Rest::json($elt); exit;
			if(!empty(Yii::app()->session["costum"]) && 
				!empty(Yii::app()->session["costum"]["pdf"]) && 
				!empty(Yii::app()->session["costum"]["pdf"][$type]) && 
				!empty(Yii::app()->session["costum"]["pdf"][$type])){
				$html = $controller->renderPartial(Yii::app()->session["costum"]["pdf"][$type], $params, true);
			} else {
				$html = $controller->renderPartial('application.views.pdf.element', $params, true);
			}
			$params["html"] = $html ;
			Pdf::createPdf($params);
		}
		
		

		
		
	}
}