<?php

class GetAction extends CAction {
/**
* Dashboard Organization
*/
    public function run($id = null, $type = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $fullRepresentation = null, $namecity=null, $level3=null, $level4=null, $subtype=null) { 

        if(Yii::app()->request->isAjaxRequest && isset(Yii::app()->session["userId"]) && !empty($type) && !empty($id)){
        	 try {
                  $res = array("result" => true, "map" =>Element::getByTypeAndId($type,$id) );
             } catch (CTKException $e) {
                  $res = array("result"=>false, "msg"=>$e->getMessage());
             }
            echo json_encode( $res );  
        } else {
          // Get format
          if( !empty(Yii::app()->session['costum'] )){
            $test = ucfirst(Yii::app()->session['costum']["slug"]);
            if(!empty($test::$dataBinding_allPoi))
              $bindMap = $test::$dataBinding_allPoi;
          }

          if( empty($bindMap) && $format == Translate::FORMAT_SCHEMA)
                $bindMap = (empty($id) ? TranslateSchema::$dataBinding_allPoi : TranslateSchema::$dataBinding_poi);
          else if ( empty($bindMap) && $format == Translate::FORMAT_KML)
            $bindMap = (empty($id) ? TranslateKml::$dataBinding_allPoi : TranslateKml::$dataBinding_poi);
          else if ( empty($bindMap) && $format == Translate::FORMAT_GEOJSON) 
            $bindMap = (empty($id) ? TranslateGeoJson::$dataBinding_allPoi : TranslateGeoJson::$dataBinding_poi);
          else if ( empty($bindMap) && $format == Translate::FORMAT_JSONFEED)
            $bindMap = TranslateJsonFeed::$dataBinding_allPoi;
            // $bindMap = (empty($id) ? TranslateJsonFeed::$dataBinding_allPoi : TranslateGeoJson::$dataBinding_poi);
          else if ( empty($bindMap) && $format == Translate::FORMAT_GOGO){
             $bindMap = ( (!empty($fullRepresentation) && $fullRepresentation == "true" ) ? TranslateGogoCarto::$dataBinding_poi : TranslateGogoCarto::$dataBinding_poi_symply);
            //$bindMap = TranslateGogoCarto::$dataBinding_poi_symply;
          }
          else if(  empty($bindMap) && ($format == Translate::FORMAT_MD || $format == Translate::FORMAT_TREE))
            $bindMap = Poi::CONTROLLER;
          else if( empty($bindMap) ){
               $bindMap = (empty($id) ? TranslateCommunecter::$dataBinding_allPoi : TranslateCommunecter::$dataBinding_poi);
          }
          $p = array();
          if(!empty($level3))
            $p["level3"] = $level3;

          if(!empty($level4))
            $p["level4"] = $level4;
          //var_dump($bindMap); exit;
          $result = Api::getData($bindMap, $format, Poi::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee, null, null, null, $namecity, $p, $subtype);
          //Rest::json($result); exit;
          if ($format == Translate::FORMAT_KML) {
            $strucKml = News::getStrucKml();    
            Rest::xml($result, $strucKml,$format);  
          } else if ($format == "csv") {
            // $res = $result["entities"];
            // $head = Export::toCSV($res, ";", "'");
             //Rest::json($result["entities"]); exit;
            Rest::csv($result["entities"], false, true, Poi::COLLECTION); 

          } else
            Rest::json($result);

          Yii::app()->end();
        }
    }
}

?>